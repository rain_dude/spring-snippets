package pl.dude.domain.item;

import pl.dude.domain.ports.ItemsRepository;

public class ItemTextConsumer {

    private final ItemsRepository itemsRepository;

    public ItemTextConsumer(ItemsRepository itemsRepository) {
        this.itemsRepository = itemsRepository;
    }


    public void consume(ItemCandidate candidate) {
        itemsRepository.save(candidate);
    }
}
