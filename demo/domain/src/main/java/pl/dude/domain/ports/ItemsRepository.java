package pl.dude.domain.ports;

import pl.dude.domain.item.Item;
import pl.dude.domain.item.ItemCandidate;
import pl.dude.domain.item.ItemId;

import java.util.List;
import java.util.Optional;

public interface ItemsRepository {

    Optional<Item> findById(ItemId id);

    default Item findByIdOrThrow(ItemId id) {
        return findById(id)
                .orElseThrow();
    }

    Item save(ItemCandidate candidate);

    List<Item> findAll();
}
