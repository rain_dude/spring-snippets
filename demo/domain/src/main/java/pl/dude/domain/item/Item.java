package pl.dude.domain.item;

public record Item(ItemId id, String text) {
}
