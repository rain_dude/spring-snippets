package pl.dude.domain.item;

import java.util.UUID;

import static java.util.Objects.requireNonNull;

public record ItemId(UUID value) {

    public ItemId {
        requireNonNull(value);
    }

    public static ItemId of(UUID value) {
        return new ItemId(value);
    }
}
