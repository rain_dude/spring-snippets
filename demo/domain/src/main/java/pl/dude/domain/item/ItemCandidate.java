package pl.dude.domain.item;

import java.util.UUID;

public record ItemCandidate(UUID id, String text) {
}
