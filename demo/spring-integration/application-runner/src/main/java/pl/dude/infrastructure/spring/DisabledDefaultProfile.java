package pl.dude.infrastructure.spring;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.type.AnnotatedTypeMetadata;

public class DisabledDefaultProfile implements Condition {

    @Override
    public boolean matches(ConditionContext context, AnnotatedTypeMetadata metadata) {
        boolean noProfilesSelected = context
                .getEnvironment()
                .getActiveProfiles()
                .length == 0;
        if (noProfilesSelected) {
            throw new IllegalStateException("Select supported profile to run application");
        }

        return true;
    }
}
