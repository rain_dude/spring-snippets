package pl.dude.infrastructure.in.rest.item;

import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.ResponseEntity;
import pl.dude.domain.item.Item;
import pl.dude.domain.item.ItemId;

public sealed interface SearchItemResponse
        permits SearchItemResponse.Found, SearchItemResponse.NotFound {

    static ResponseEntity<SearchItemResponse> entityFrom(Item item) {
        return ResponseEntity
                .ok(Found.from(item));
    }

    static ResponseEntity<SearchItemResponse> entityOfNotFound() {
        return ResponseEntity
                .ok(new NotFound());
    }

    record Found(ItemId id, String text) implements SearchItemResponse {

        public static Found from(Item item) {
            return new Found(item.id(), item.text());
        }


        @JsonProperty("id")
        @Override
        public ItemId id() {
            return id;
        }

        @JsonProperty("text")
        @Override
        public String text() {
            return text;
        }
    }

    record NotFound() implements SearchItemResponse {

        @JsonProperty("message")
        public String message() {
            return "not found";
        }
    }
}
