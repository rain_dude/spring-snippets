package pl.dude.domain.commons;

import java.time.LocalDateTime;
import java.util.function.Supplier;

public interface LocalDateTimeProvider extends Supplier<LocalDateTime> {

    default LocalDateTime now() {
        return get();
    }
}
