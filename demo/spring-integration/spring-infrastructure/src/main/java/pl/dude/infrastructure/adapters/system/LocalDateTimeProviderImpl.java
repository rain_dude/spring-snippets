package pl.dude.infrastructure.adapters.system;

import org.springframework.stereotype.Component;
import pl.dude.domain.commons.LocalDateTimeProvider;

import java.time.LocalDateTime;

@Component
public class LocalDateTimeProviderImpl implements LocalDateTimeProvider {

    @Override
    public LocalDateTime get() {
        return LocalDateTime
                .now();
    }
}
