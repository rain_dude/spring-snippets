package pl.dude.infrastructure.adapters.item;

import org.springframework.stereotype.Component;
import pl.dude.domain.commons.LocalDateTimeProvider;
import pl.dude.domain.item.Item;
import pl.dude.domain.item.ItemCandidate;
import pl.dude.domain.item.ItemId;
import pl.dude.domain.ports.ItemsRepository;
import pl.dude.infrastructure.out.mongo.item.ItemDocument;
import pl.dude.infrastructure.out.mongo.item.ItemsMongoRepository;

import java.util.List;
import java.util.Optional;

@Component
public class ItemsRepositoryAdapter implements ItemsRepository {

    private final ItemsMongoRepository mongoRepository;
    private final LocalDateTimeProvider localDateTimeProvider;

    public ItemsRepositoryAdapter(ItemsMongoRepository mongoRepository, LocalDateTimeProvider localDateTimeProvider) {
        this.mongoRepository = mongoRepository;
        this.localDateTimeProvider = localDateTimeProvider;
    }


    @Override
    public Optional<Item> findById(ItemId id) {
        return mongoRepository
                .findById(id.value())
                .map(ItemDocument::toItem);
    }

    @Override
    public List<Item> findAll() {
        return mongoRepository
                .findAll()
                .stream()
                .map(ItemDocument::toItem)
                .toList();
    }

    @Override
    public Item save(ItemCandidate candidate) {
        var document = ItemDocument
                .from(candidate)
                .setTimestamp(localDateTimeProvider.now());
        ItemDocument saved = mongoRepository
                .save(document);
        return saved
                .toItem();
    }
}
