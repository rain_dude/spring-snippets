package pl.dude.infrastructure.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.dude.domain.commons.LocalDateTimeProvider;
import pl.dude.domain.item.ItemTextConsumer;
import pl.dude.domain.ports.ItemsRepository;
import pl.dude.infrastructure.adapters.item.ItemsRepositoryAdapter;
import pl.dude.infrastructure.out.mongo.item.ItemsMongoRepository;

@Configuration
public class ItemDomainConfiguration {

    public ItemDomainConfiguration() {
    }


    @Bean
    public ItemsRepository itemsRepository(ItemsMongoRepository mongoRepository, LocalDateTimeProvider localDateTimeProvider) {
        return new ItemsRepositoryAdapter(mongoRepository, localDateTimeProvider);
    }

    @Bean
    public ItemTextConsumer itemTextConsumer(ItemsRepository itemsRepository) {
        return new ItemTextConsumer(itemsRepository);
    }
}
