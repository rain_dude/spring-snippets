package pl.dude.infrastructure.spring.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import pl.dude.domain.StringPrinter;
import pl.dude.domain.ports.ItemsRepository;
import pl.dude.infrastructure.adapters.item.ItemsRepositoryAdapter;
import pl.dude.infrastructure.out.mongo.item.ItemsMongoRepository;

@Configuration
public class DomainConfiguration {

    public DomainConfiguration() {
    }


    @Bean
    public StringPrinter stringPrinter() {
        return new StringPrinter();
    }
}
