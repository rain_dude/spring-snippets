package pl.dude.infrastructure.in.rest.item;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import pl.dude.domain.StringPrinter;
import pl.dude.domain.item.ItemId;
import pl.dude.domain.ports.ItemsRepository;

import java.util.List;
import java.util.UUID;

@RestController
public class ItemsRestController {

    private final StringPrinter stringPrinter;
    private final ItemsRepository itemsRepository;

    public ItemsRestController(StringPrinter stringPrinter, ItemsRepository itemsRepository) {
        this.stringPrinter = stringPrinter;
        this.itemsRepository = itemsRepository;
    }


    @GetMapping("string")
    public String getString() {
        return stringPrinter
                .print();
    }

    @GetMapping("item/{id}")
    public ResponseEntity<SearchItemResponse> itemResponse(@PathVariable("id") UUID idValue) {
        return itemsRepository
                .findById(ItemId.of(idValue))
                .map(SearchItemResponse::entityFrom)
                .orElse(SearchItemResponse.entityOfNotFound());
    }

    @GetMapping("items")
    public ResponseEntity<List<SearchItemResponse>> itemResponse() {
        List<SearchItemResponse> founds = itemsRepository
                .findAll()
                .stream()
                .map(SearchItemResponse.Found::from)
                .map(x -> (SearchItemResponse) x)
                .toList();
        return ResponseEntity
                .ok(founds);
    }
}
