package pl.dude.infrastructure.in.kafka.item;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;
import pl.dude.domain.item.ItemCandidate;
import pl.dude.domain.item.ItemTextConsumer;

import java.util.UUID;

@Component
public class KafkaConsumer {

    private static final Logger logger = LoggerFactory.getLogger(KafkaConsumer.class);


    private final ItemTextConsumer itemTextConsumer;

    public KafkaConsumer(ItemTextConsumer itemTextConsumer) {
        this.itemTextConsumer = itemTextConsumer;
    }


    @KafkaListener(topics = "my-topic")
    public void consume(String message) {
        itemTextConsumer
                .consume(new ItemCandidate(UUID.randomUUID(), message));
    }
}

