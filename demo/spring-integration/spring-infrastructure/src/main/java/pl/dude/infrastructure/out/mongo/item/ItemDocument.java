package pl.dude.infrastructure.out.mongo.item;

import com.google.common.base.MoreObjects;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.core.mapping.Field;
import pl.dude.domain.item.Item;
import pl.dude.domain.item.ItemCandidate;
import pl.dude.domain.item.ItemId;

import java.time.LocalDateTime;
import java.util.UUID;

@Document("item")
public class ItemDocument {

    @Id
    @Field("id")
    private UUID id;
    @Field("timestamp")
    private LocalDateTime timestamp;

    @Field("text")
    private String text;

    public ItemDocument() {
    }

    public static ItemDocument from(ItemCandidate candidate) {
        return new ItemDocument()
                .setId(candidate.id())
                .setText(candidate.text());
    }


    public Item toItem() {
        return new Item(new ItemId(id), text);
    }


    public UUID getId() {
        return id;
    }

    public ItemDocument setId(UUID id) {
        this.id = id;
        return this;
    }

    public LocalDateTime getTimestamp() {
        return timestamp;
    }

    public ItemDocument setTimestamp(LocalDateTime timestamp) {
        this.timestamp = timestamp;
        return this;
    }

    public String getText() {
        return text;
    }

    public ItemDocument setText(String text) {
        this.text = text;
        return this;
    }


    @Override
    public String toString() {
        return MoreObjects.toStringHelper(this)
                .add("id", id)
                .add("text", text)
                .toString();
    }
}
