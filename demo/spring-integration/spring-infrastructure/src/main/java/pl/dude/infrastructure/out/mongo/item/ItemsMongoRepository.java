package pl.dude.infrastructure.out.mongo.item;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import java.util.UUID;

@Repository
public interface ItemsMongoRepository extends MongoRepository<ItemDocument, UUID> {
}
