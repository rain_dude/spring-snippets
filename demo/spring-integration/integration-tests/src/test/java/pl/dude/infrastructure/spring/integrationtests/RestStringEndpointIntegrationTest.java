package pl.dude.infrastructure.spring.integrationtests;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import pl.dude.infrastructure.adapters.system.LocalDateTimeProviderImpl;
import pl.dude.infrastructure.in.rest.item.ItemsRestController;
import pl.dude.infrastructure.out.mongo.item.ItemsMongoRepository;
import pl.dude.infrastructure.spring.config.DomainConfiguration;
import pl.dude.infrastructure.spring.config.ItemDomainConfiguration;

import static org.hamcrest.Matchers.is;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(classes = {
        DomainConfiguration.class, ItemDomainConfiguration.class, LocalDateTimeProviderImpl.class,
        ItemsRestController.class
})
@AutoConfigureMockMvc
class RestStringEndpointIntegrationTest {

    @Autowired
    MockMvc mockMvc;

    @MockBean
    ItemsMongoRepository itemsMongoRepository;


    @Test
    void string_WhenCalled_ReturnsExpectedResponse() throws Exception {
        mockMvc
                .perform(get("/string"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", is("asdf blu blu")));
    }
}
